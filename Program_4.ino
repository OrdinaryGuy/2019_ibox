
void progCore4() {
  if (bitRead(setupByte, 3) == 0) {
    bitSet(setupByte, 3);

    // zabráni nechcenému preskoku medzi programami po ich spustení
    CLKwise = false;
    antiCLKwise = false;
  }
  if (millis() % 1000 == 0) {
    change = true;
  }
}

void progDisp4() {
  unsigned int voltage = 0;
  float battVoltage = 0;
   const float Au = 0.7063;
   const float Bu = 745.38;
   const float Ad = 7.208;
   const float Bd = 688.9;
   float Xu;
   float Xd;
   byte battery;
   static byte secCount;
  
  // Strednou hodnotou 30 meraní získame stabilnejšiu hodnotu napätia (overflov pri viac ako 60 kumulácii)
  
  secCount++;

  for (int i = 0; i < 30; i++) {
    voltage += analogRead (battPin);
  }
  voltage = voltage / 30;
  battVoltage = (5.0 * voltage)/1023.0;
  battVoltage *= 1000;

  Xu = ((voltage - Bu) / Au);
  Xd = (voltage - Bd) / Ad;
  battery = max(Xu, Xd);

  if (battery > 100) {
    battery = 100; 
    analogWrite(green, 10);
    analogWrite(red, 0);
    analogWrite(blue, 0);
  }
  else if (battery > 50) {
    analogWrite(blue, 0);
    analogWrite(red, 20);
    analogWrite(green, 4);
  }
  else {
    analogWrite(red, 20);
    analogWrite(green, 0);
    analogWrite(blue, 0);
  }

    if (secCount <= 5) {
      //display.clear();
      if (secCount == 1) {display.setSegments(0);}
      display.showNumberDec (battery, false, 3, 0);
    }
    else if (secCount < 10) {
      //display.clear();
      display.showNumberDec (battVoltage, false);
    }
    else {
      secCount = 0;
    }
}

void progCore5() {
  
}

void progDisp5() {
  
}

void progCore6() {
  
}

void progDisp6() {
  
}

void progCore7() {
  
}

void progDisp7() {
  
}
