  int hodProg2;
  int minProg2;
  bool tickProg2 = false;

void progCore2() {
static int secProg2;
static unsigned long startProg2;
static unsigned long countProg2;
  
  // Setup programu 2
  if (bitRead(setupByte, 1) == 0) {
    bitSet(setupByte, 1);
    clickCount = 0;
    while (clickCount <= 5) {
      //rotaryEncoder();
      //progCores();
      switch (clickCount) {
        case 0:
        value = 0;
        minValue = 0;
        maxValue = 23;
        stepUp = 5;
        stepDown = 1;        
        clickCount++;
        animationTime = millis();
        break;
        case 1:
        setAnimation();
        change = true;
        break;
        case 2:
        if (change == true) {
          change = false;
          uint8_t disp2[] = { 0, 0, 64, 64 };
          disp2[0] = display.encodeDigit(value/10);
          disp2[1] = display.encodeDigit(value%10);
          disp2[1] += 128;
          display.setSegments(disp2);
        }
        break;
        case 3:
        hodProg2 = value;
        value = 0;
        maxValue = 59;
        animationTime = millis();
        clickCount++;
        break;
        case 4:
        setAnimation();
        change = true;
        break;
        case 5:
        if (change == true) {
          change = false;
          uint8_t disp2[] = { 64, 192, 0, 0 };
          disp2[2] = display.encodeDigit(value/10);
          disp2[3] = display.encodeDigit(value%10);
          display.setSegments(disp2);
        }
        break;
      } 
    }
    CLKwise = false;
    antiCLKwise = false;
    minProg2 = value;
    value = 0;
    SWpush = false;
    secProg2 = 0;
    countProg2 = 1;
    startProg2 = millis();
    change = true;
  }

  // Main pre Program 1
  if (millis() - startProg2 >= countProg2 * 500) {
    countProg2++;
    tickProg2 = !tickProg2;
    if (tickProg2 == true) {
      secProg2++;
      if (secProg2 > 59) {
        secProg2 = 0;
        minProg2++;
      }
      if (minProg2 > 59) {
        minProg2 = 0;
        hodProg2++;
      }
      if (hodProg2 > 23) {
        hodProg2 = 0;
      }
    } 
    if (dispByte == 0b10) {change = true;}
  }
}

void progDisp2() {
  uint8_t disp2[] = { };
  disp2[0] = display.encodeDigit(hodProg2/10);
  disp2[1] = display.encodeDigit(hodProg2%10);
  if (tickProg2 == true) {disp2[1] += 128;}
  disp2[2] = display.encodeDigit(minProg2/10);
  disp2[3] = display.encodeDigit(minProg2%10);
  display.setSegments(disp2);
}
