int secProg3;

void progCore3() {
static int startSecProg3;
static int stopSecProg3;
static unsigned long startProg3;
static byte countProg3;
  
// Setup programu 2
  if (bitRead(setupByte, 2) == 0) {
    bitSet(setupByte, 2);
    clickCount = 0;
    countProg3 = 0;
    Serial.println("Setup Pr. 3");
    while (clickCount <= 1) {
      //rotaryEncoder();
      //progCores();
        if (clickCount == 0) {
          // Hodnoty budú 10x väčie
          value = 0;
          minValue = 0;
          maxValue = 990;
          stepUp = 50;
          stepDown = 10;        
          clickCount++;
          uint8_t disp3[] = { 63, 191, 63, 63 }; // Zobrazí "00:00"
          display.setSegments(disp3);
          Serial.println ("case 0");
        }
        if (clickCount == 1) {
        
        if (change == true) {
          change = false;
          display.showNumberDecEx(10 * value, 0b01000000, true);
        }
      }
    }
    CLKwise = false;
    antiCLKwise = false;
    startSecProg3 = 10 * value;
    value = 0;
    SWpush = false;
    countProg3 = 1;
  }

  // Main pre Program 3

  if (SWpush == true && dispByte == 0b100) {
    SWpush = false;
    countProg3++;
  }

    if (countProg3 == 1) {
      startProg3 = millis();
      countProg3++;
    }
    else if (countProg3 == 2) {
      secProg3 = startSecProg3 - ((millis() - startProg3)/10);
      if (secProg3 < 0) {secProg3 = 0; countProg3++; change = true;}
      if (dispByte == 0b100 && secProg3 >= 0) {change = true;}
      }
    else if (countProg3 == 3) {
      stopSecProg3 = secProg3;
      countProg3++;
    }
    else if (countProg3 == 4 && dispByte == 0b100) {
      display.showNumberDecEx(stopSecProg3, 0b01000000, true);
    }
    else if (countProg3 == 5) {
      secProg3 = startSecProg3;
      display.showNumberDecEx(secProg3, 0b01000000, true);
      countProg3 = 0;
    }

  static int delTime;
  static unsigned long actualDelay;
  static byte prog3Step = 0;
  if (dispByte == 0b100 && countProg3 == 2 && digitalRead(SW) == HIGH) {
    delTime = (970.0/startSecProg3) * secProg3 + 30.0;
    if (prog3Step == 0) {
      actualDelay = millis();
      prog3Step++;
    }
    else if (prog3Step == 1) {
      if ((millis() - actualDelay) >= delTime) {
        analogWrite (red, 40);
        analogWrite (blue, 60);
        prog3Step++;
      }
    }
    else if (prog3Step == 2) {
      if (millis() - actualDelay >= delTime + 10) {
        analogWrite (red, 0);
        analogWrite (blue, 0);
        prog3Step = 0;
      }
    } 
  }
  if (dispByte == 0b100 && (countProg3 == 3 || countProg3 == 4) && stopSecProg3 == 0 && digitalRead(SW) == HIGH) {
    analogWrite (red, 20);
    analogWrite (blue, 30);
  }
  if (dispByte == 0b100 && countProg3 == 5 || digitalRead(SW) == LOW) {
    analogWrite (red, 0);
    analogWrite (blue, 0);
  }
}

void progDisp3() {
  display.showNumberDecEx(secProg3, 0b01000000, true);
}
