  int minProg1;
  int secProg1;
  bool tickProg1 = false;
  bool holdProg1;
  unsigned long animationTime;
  
void progCore1() {
  static int decrementProg1;
  static unsigned long startProg1;
  static unsigned long countProg1;

  // Setup programu 1
  if (bitRead(setupByte, 0) == 0) {
    bitSet(setupByte, 0);
    clickCount = 0;
    minProg1 = 0;
    secProg1 = 0;
    decrementProg1 = 0;
    while (clickCount <= 8) {
      //rotaryEncoder();
      //progCores();
      switch (clickCount) {
        // Nastavenie počiatočnej hodnoty minút
        case 0:
        value = 0;
        minValue = 0;
        maxValue = 99;
        stepUp = 5;
        stepDown = 1;
        clickCount++;
        animationTime = millis();
        break;
        case 1:
        setAnimation();
        break;
        case 2:
        if (change == true) {
          change = false;
          uint8_t disp1[] = { 0, 0, 64, 64 };
          cli();
          disp1[0] = display.encodeDigit(value/10);
          disp1[1] = display.encodeDigit(value%10);
          display.setSegments(disp1);
          sei();
        }
        break;
        case 3:
        Serial.println("Min: ");
        Serial.println(value);
        minProg1 = value;
        value = 0;
        maxValue = 59;
        animationTime = millis();
        clickCount++;
        break;
        // Nastavenie počiatočnej hodnoty sekúnd
        case 4:
        cli();
        setAnimation();
        change = true;
        sei();
        break;
        case 5:
        if (change == true) {
          change = false;
          uint8_t disp1[] = { 64, 64, 0, 0 };
          disp1[2] = display.encodeDigit(value/10);
          disp1[3] = display.encodeDigit(value%10);
          cli();
          display.setSegments(disp1);
          sei();
          Serial.println(minProg1);
        }
        break;
        // Nastavenie hodnoty odpočtu minú pri stlačení
        case 6:
        Serial.println("Sek: ");
        secProg1 = value;
        value = 1;
        minValue = 1;
        maxValue = 99;
        stepUp = 1;
        animationTime = millis();
        clickCount++;
        break;
        case 7:
        cli();
        setAnimation();
        change = true;
        sei();
        break;
        case 8:
        if (change == true) {
          change = false;
          uint8_t disp1[] = { 64, 0, 0, 64 };
          cli();
          disp1[1] = display.encodeDigit(value/10);
          disp1[2] = display.encodeDigit(value%10);
          display.setSegments(disp1);
          sei();
          Serial.println(secProg1);
        }
        break;
      }
    }
    CLKwise = false;
    antiCLKwise = false;
    decrementProg1 = value;
    SWpush = false;
    holdProg1 = false;
    countProg1 = 1;
    startProg1 = millis();
    change = true;
        Serial.print("Min: ");
        Serial.println(minProg1);
        Serial.print("Sek: ");
        Serial.println(secProg1);
        Serial.print("Dek: ");
        Serial.println(decrementProg1);
  }

  // Main pre Program 1
  if (SWpush == true && dispByte == 0b1) {
    SWpush = false;
    if (holdProg1 == false){
      minProg1 -= decrementProg1;
      if (minProg1 < 0) {
        minProg1 = abs(minProg1) - 1;
        secProg1 = 60 - secProg1;
        holdProg1 = true;
      }
    }
    else {
      minProg1 += decrementProg1;
    }
    if (dispByte == 0b1) {change = true;}
  }
  
  if (millis() - startProg1 >= countProg1 * 500 && decrementProg1 != 0) {
    Serial.println("Idem");
    countProg1++;
    tickProg1 = !tickProg1;
    if (holdProg1 == false && tickProg1 == true) {
      secProg1--;
      if (secProg1 < 0) {
        secProg1 = 59;
        minProg1--;
      }
      if (minProg1 < 0) {
        secProg1 = minProg1 = 0;
        holdProg1 = true;
      }
    }
    else if (holdProg1 == true && tickProg1 == true) {
      secProg1++;
      if (secProg1 > 59) {
        secProg1 = 0;
        minProg1++;
      }
    }
    if (dispByte == 0b1) {change = true;}
  }
}

void progDisp1() {
  uint8_t disp1[] = { };
  disp1[0] = display.encodeDigit(minProg1/10);
  if (holdProg1 == true && minProg1 < 10){disp1[0] = 64;}
  disp1[1] = display.encodeDigit(minProg1%10);
  if (tickProg1 == true && holdProg1 == false) {disp1[1] += 128;}
  else if (tickProg1 == true && holdProg1 == true) {disp1[1] += 128; analogWrite(red, 10);}
  else {analogWrite(red, 0);}
  disp1[2] = display.encodeDigit(secProg1/10);
  disp1[3] = display.encodeDigit(secProg1%10);
  display.setSegments(disp1);
}

void mainMenu() {
  //program setup
  if (bitRead(setupByte, 7) == 0) {
    bitSet(setupByte, 7);
    minValue = 1;
    maxValue = numOfPrograms;
    stepUp = 1;
    stepDown = 1;
    uint8_t dispMenu[] = { 115, 80 };
    dispMenu[2] = display.encodeDigit(value/10);
    dispMenu[3] = display.encodeDigit(value%10);
    display.setSegments(dispMenu);
  }
}

void dispMenu() {
  
  uint8_t dispMenu[] = { 115, 80 };
  dispMenu[2] = display.encodeDigit(value/10);
  dispMenu[3] = display.encodeDigit(value%10);
  display.setSegments(dispMenu);
  dispByte = 0b10000000;
}

void mainMenuLED() {
static const byte amplitude = 10;
static const byte offset = 10;
static const unsigned int period = 1300;
static float led;
static unsigned long animationTime;
static byte lastDispByte = 1;

  if (dispByte != lastDispByte) {
    lastDispByte = dispByte;
    animationTime = millis();
  }
  
  if (dispByte == 0b10000000 && digitalRead(SW) == HIGH) {
    
    if (millis() % 10 == 0) {
      led = amplitude * sin(6.2831853*(millis() - animationTime)/period-1.5708)+offset;
      if (led < 0) {
        led = 0;
      }
    }
    if (bitRead(progByte, value - 1)) {
      
        analogWrite(green, 0);
        analogWrite(blue, led);
    }
    else {
      analogWrite (blue, 0);
      analogWrite (green, led/2);
    }
  }
}

// Úvodné animácie pre programy 1 a 2
void setAnimation() {
static byte introSetA [] = { 1, 0, 0, 0, 8, 24, 48, 33, 1, 1, 1, 1, 9, 25, 61, 63};
static byte introSetB [] = { 1, 3, 6, 12, 8, 0, 0, 0, 1, 3, 7, 15, 15, 15, 15, 31}; //i < 17
static byte i = 0;

  if (millis() - animationTime > 5*i + 1 && i < 17) {
      byte valA = introSetA[i];
      byte valB = introSetB[i];
      if (clickCount == 1) {
        uint8_t disp[] = { valA, valB, 64, 64 };    
        display.setSegments(disp);
        analogWrite(red, i/2);
      }
      else if (clickCount == 4) {
        analogWrite(red, sizeof(introSetA)/2);
        uint8_t disp[] = { 64, 64, valA, valB };    
        display.setSegments(disp);
        analogWrite(green, i/2);
      }
      else if (clickCount == 7) {
        analogWrite(red, sizeof(introSetA)/2);
        analogWrite(green, sizeof(introSetA)/2);
        uint8_t disp[] = { 64, valA, valB, 64 };    
        display.setSegments(disp);
        analogWrite(blue, i/2);
      }
      i++;
    }
  else if (i >= 17){
    i = 0;
    clickCount++;
  }
}
