#include <Arduino.h>
#include <TM1637Display.h>

// Piny pre display (all Digital Pins)
#define dCLK 4
#define DIO  5

// Inicializácia display-a
TM1637Display display(dCLK, DIO);

//Piny pre RGB LED
#define blue   9
#define green  10
#define red    6

//Piny pre rotačný encoder
#define SW   8
#define DT   7
#define eCLK 11

//Piny monitoringu batérie
#define powSwitch A0
#define battPin   A1
#define chrg      A2
  
//Globálne premenné pre Programy
const byte numOfPrograms = 4; // Počet programov (1 - 7)

//Globálne premenné pre main program
byte progByte = 0b10000000;   //Všetky programy sú vypnuté
byte dispByte = 0b10000000;   //Všetky bity = 0, sme v hlavnom Menu
byte setupByte = 0;           //Bit s 1 prešiel setupom

//Globálne premenné pre encoder
int value = 1;                // 1 pri štarte
int minValue;                 // Minimálna hodnota pre encoder v závislosti od programu
int maxValue;                 // Maximálna hodnota pre encoder v závislosti od programu
byte stepUp;                  // Zmena hodnoty "value" pre CLK krok
byte stepDown;                // Zmena hodnoty "value" pre aniCLK krok
bool CLKwise = false;         // Otočenie v CLK smere
bool antiCLKwise = false;     // Otočenie v antiCLK smere
byte SWclick = 0;             // Registrácia stlačenia
byte clickCount = 0;          // Počet stlačení
bool change = true;           //Zobrazenie hl. Menu pri štarte

bool DTlastState;             // Posledné pozície encodera
bool SWlastState = HIGH;

void setup() {
  Serial.begin(9600);
  
  // Inicializácia RGB pinov
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  pinMode(blue, OUTPUT);

  // Inicializácia battery management pinov
  pinMode(chrg, INPUT);
  pinMode(powSwitch, INPUT);
  pinMode(battPin, INPUT);

  // Inicializácia pinov encodera
  pinMode(SW, INPUT_PULLUP); //encoder HW 040 nemá svoj vlastný pullup rezistor pre SW
  pinMode(DT, INPUT_PULLUP);
  pinMode(eCLK, INPUT_PULLUP);

  // Vymazanie a nastavenie jasu display-a
  display.clear();
  display.setBrightness(1);

  // Úvodná pozícia encodera
  DTlastState = digitalRead(DT);

  // Nastavenie Timer 2 ako časový interrupt
  cli();
  TCCR2A = 0;     // set entire TCCR1A register to 0
  TCCR2B = 0;     // same for TCCR1B
  TCNT2  = 0;     //initialize counter value to 0;
  OCR2A = 255;    // = (16*10^6) / (1000*8) - 1
  TCCR2A |= (1 << WGM21);    // turn on CTC mode
  TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20);   // Set CS11 bit for 8 prescaler
  TIMSK2 |= (1 << OCIE2A);   // enable timer compare interrupt
  sei();

  introAnimation();
  battSetUp();
  battStatus();
}

void loop() {
  progCores();
  dispOutput();
  mainMenuLED();
}

//-------------------------- Jadro programu --------------------------------
//--------------------------------------------------------------------------
// Má na starosti logiku spustených programov a základné zobrazovanie.
// Update zobrazenia má na starosti funkcia dispOutput()
void progCores() {
    // Jadro programov 1 - 7
  if (bitRead(progByte, 0) == true) {
    progCore1();
  }
  if (bitRead(progByte, 1) == true) {
    progCore2();
  }
  if (bitRead(progByte, 2) == true) {
    progCore3();
  }
  if (bitRead(progByte, 3) == true) {
    progCore4();
  }
  if (bitRead(progByte, 4) == true) {
    progCore5();
  }
  if (bitRead(progByte, 5) == true) {
    progCore6();
  }
  if (bitRead(progByte, 6) == true) {
    progCore7();
  }
  // Jadro hl. Menu
  if (bitRead(progByte, 7) == true) {
    mainMenu();
  }
}

bool SWpush;              // Aktivujú sa krátkym stlačením
ISR(TIMER2_COMPA_vect){
  static const int longPulse = 1200;
  static const int shortPulse = 300;
  static const int ignorePulse = 20;
  static unsigned long SWtime;

  //----------------- Rotácia encodera -------------------------
  //------------------------------------------------------------
  if (digitalRead(DT) != DTlastState){
    DTlastState = !DTlastState;
    //delay(3);  

    // Podmienky CLK a antiCLK rotácie
    bool eCLKstate = digitalRead(eCLK);
    if (eCLKstate != DTlastState) {
      value -= stepDown;
      if (dispByte != 0b10000000) {antiCLKwise = true;}
    }
    else if (eCLKstate == DTlastState) { // Smer hodinových ručičiek
      value += stepUp;
      if (dispByte != 0b10000000) {CLKwise = true;}
    }

    // Hraničné podmienky rotácie encodera
    if (value > maxValue) {value = minValue;}
    else if (value < minValue) {value = maxValue;}

    Serial.println(value);
    change = true; // Zmena sa zobrazí na display
  }

  //---------------------- Vypínač -----------------------------
  //------------------------------------------------------------
  // Stlačenie vypínača
  bool SWstate = digitalRead(SW);
  if (SWstate == LOW && SWstate != SWlastState) {
    SWlastState = !SWlastState;
    SWtime = millis();
    SWclick++;
  }

  // LED pre typ stlačenia
  if (SWstate == LOW){
    if (millis() - SWtime < shortPulse) {
      analogWrite(green, 2);
    }
    else if (millis() - SWtime < longPulse) {
      analogWrite(green, 0); 
      analogWrite(blue, 4);
    }
    else {
      analogWrite(blue, 0); 
      analogWrite(red, 4);
    }
  }

  // Uvoľnenie vypínača
  else if (SWstate == HIGH && SWstate != SWlastState){
    SWlastState = !SWlastState;
    delay(5);
    
    analogWrite(red, 0);
    analogWrite(green, 0);
    analogWrite(blue, 0);

    // Podmienky pre typ stlačenia
    //------------------------ Krátky impulz --------------------------------
    if (millis() - SWtime > ignorePulse && millis() - SWtime < shortPulse) {
          
      // Hl. menu
      if (dispByte == 0b10000000) {
        bitClear(dispByte, 7);
        bitClear(progByte, 7); //Ukončí program pre hl. Menu
        bitSet(dispByte, (value - 1));
        bitSet(progByte, (value - 1));
        change = true;
      }
      else {
        clickCount++;
        SWpush = true;
      }
      
    }

    //------------------------ Návrat do hl. Menu -----------------------------
    else if (millis() - SWtime >= shortPulse && millis() - SWtime < longPulse && dispByte != 0b10000000) { 
      
      // Funkcia určí, ktorý bit v dispByte je "1"
      bool control_bit = false;
      byte bitPosition = 0;
      while (control_bit == false) {
        control_bit = (dispByte & (1 << bitPosition));
        bitPosition++;
      }

      // Nastavenia pre vstup do hl. Menu
      value = bitPosition;
      dispByte = 0b10000000;  // Zobrazenie hl. menu
      bitSet(progByte, 7);    // Aktivácia programu hl. Menu
      bitClear(setupByte, 7); // Aktivácia setup-u hl. Menu
      change = true;
      }
      SWtime = 0 - 1;
      SWclick = 0;
  }

  //---------------------- Podmienka dlhého stlačenia ---------------------------
  //--------------------- Vypnutie aktuálneho programu --------------------------
  if (SWclick == 1 && millis() - SWtime >= longPulse) { 
    
    // Program funguje iba ak nie sme v hl. Menu
    if (dispByte != 0b10000000) {
    progByte &= ~dispByte;  // Vypne aktuálny program
    setupByte &= ~dispByte; // Aktivuje jeho setup pre budúcnosť
  
    // Funkcia určí, ktorý bit v dispByte je "1"
    bool control_bit = false;
    byte bitPosition = 0;
    while (control_bit == false) {
      control_bit = (dispByte & (1 << bitPosition));
      bitPosition++;
    }
    
    // Nastavenia pre vstup do hl. Menu
    value = bitPosition;
    dispByte = 0b10000000; //Zobrazenie hl. Menu
    bitSet(progByte, 7);    //Zapne program pre hl. Menu
    bitClear(setupByte, 7);
    Serial.print("reset");
    change = true;          //Aktivácia funkcie dispProgram()
    } 
    SWtime = 0 - 1; 
    SWclick = 0;
  }
  //progCores();
}

//------------------ Funkcia zobrazovania na display ----------------------------
//-------------------------------------------------------------------------------
void dispOutput(){
  
  //-------------- Funkcia prebehne iba ak nastane zmena ------------------------
  if (change == true) {
    // Zabezpečenie zobrazenia iba jednej funkcie v daný čas
    if (bitRead(dispByte, 0) == 1){
      progDisp1();
    }
    else if (bitRead(dispByte, 1) == 1){
      progDisp2();
    }
    else if (bitRead(dispByte, 2) == 1){
      progDisp3();
    }
    else if (bitRead(dispByte, 3) == 1){
      progDisp4();
    }
    else if (bitRead(dispByte, 4) == 1){
      progDisp5();
    }
    else if (bitRead(dispByte, 5) == 1){
      progDisp6();
    }
    else if (bitRead(dispByte, 6) == 1){
      progDisp7();
    }
    else if (bitRead(dispByte, 7) == 1){
      dispMenu();
    }
    change = false; // Deaktivácia zmeny po vykonaní
  }

  //------- Funkcia zabezpečujúca prechod dispOutput-u medzi bežiacimi programami -----
  //-----------------------------------------------------------------------------------
  
  if (dispByte != 0b10000000) {

    bool position_bit = false;
    if (CLKwise == true) {
      CLKwise = false;

      while (position_bit == false) {
        if (dispByte == 0b1000000) { dispByte = 0b1;}
        else {dispByte = (dispByte << 1);}
        position_bit = progByte & dispByte;
      }
      change = true;
      analogWrite (red, 0);
      analogWrite (green, 0);
      analogWrite (blue, 0);
    }
    else if (antiCLKwise == true) {
      antiCLKwise = false;
      
      while (position_bit == false) {
        if (dispByte == 0b1) { dispByte = 0b1000000;}
        else {dispByte = (dispByte >> 1);}
        position_bit = progByte & dispByte;
      }
      change = true;
      analogWrite (red, 0);
      analogWrite (green, 0);
      analogWrite (blue, 0);
    }
  }
}

 void battStatus () {
   unsigned int voltage = 0;
   const float Au = 0.7063;
   const float Bu = 745.38;
   const float Ad = 7.208;
   const float Bd = 688.9;
   float Xu;
   float Xd;
   byte battery;
  
  // Strednou hodnotou 30 meraní získame stabilnejšiu hodnotu napätia (overflov pri viac ako 60 kumulácii)
  for (int i = 0; i < 30; i++) {
    voltage += analogRead (battPin);
  }
  voltage = voltage / 30;

  Xu = ((voltage - Bu) / Au);
  Xd = (voltage - Bd) / Ad;
  battery = max(Xu, Xd);

  if (battery > 100) {battery = 100; analogWrite(green, 10);}
  else if (battery > 50) {analogWrite(blue, 20);}
  else {analogWrite(red, 20);}

  display.showNumberDec (battery, false, 3, 0);
  delay (2000);
  display.clear();
  analogWrite(red, 0);
  analogWrite(green, 0);
  analogWrite(blue, 0);
  delay (400);
 }

 void battSetUp() {
  int voltage;
  
  //Potrebné pre správnosť merania pinu
  for (int i = 0; i < 3000; i++){
    voltage = analogRead(battPin);
  }
}

void introAnimation() {
  byte val;
byte introMem [] = {1, 3, 6, 12, 24, 48, 33};

byte introA [] = {0, 0, 0, 0, 0, 8,24,56,57,57,57,57,57,57,57,57,121,127,127,127,127,127,127,127};
byte introB [] = {0, 0, 0, 0, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9,73, 73, 73,121,127,127,127,127,255};
byte introC [] = {0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9,73,73, 73, 73, 73, 73,121,127,127,127};
byte introD [] = {3, 6,12,12,12,12,12,12,12,12,12,13,15,79,79,79, 79, 79, 79, 79, 79, 79,127,127};


display.setBrightness(1);
  uint8_t intro[] = { 127, 255, 127, 127 }; 
  randomSeed(analogRead(A7));
  for (int j = 0; j < 4; j++) {
    analogWrite(red, 20 * random(0,2));
    analogWrite(green, 15 * random(0,2));
    analogWrite(blue, 20 * random(0,2));
  for (int i = 0; i < 7;  i++) {
  val = introMem[i];
  if (j == 0) { uint8_t intro[] = { val, 0, 0, 0 };display.setSegments(intro);}
  else if (j == 1) { uint8_t intro[] = { 0, val, 0, 0 };display.setSegments(intro);}
  else if (j == 2) { uint8_t intro[] = { 0, 0, val, 0 };display.setSegments(intro);}
  else if (j == 3) { uint8_t intro[] = { 0, 0, 0, val };display.setSegments(intro);}
  delay(15);
  }
  }
  analogWrite(red, 55);
  analogWrite(green, 55);
  analogWrite(blue, 55);
  for (int i = 0; i <= 23; i++) {
    byte valA = introA[i];
    byte valB = introB[i];
    byte valC = introC[i];
    byte valD = introD[i];
    uint8_t intro[] = { valA, valB, valC, valD };
    display.setSegments(intro);
    delay(12);
  }
  delay (400);
  display.clear();
  analogWrite(red, 0);
  analogWrite(green, 0);
  analogWrite(blue, 0);
  delay (300);
}
